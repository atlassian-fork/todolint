package todolint

import (
	"bufio"
	"fmt"
	"os"
	"regexp"
)

// Line represents a single line from a file.
type Line struct {
	// Name is the file path currently being iterated over.
	Name string
	// Number is the line number that the text falls on.
	Number uint64
	// Text is the content plucked from a file.
	Text string
}

func (l Line) String() string {
	return fmt.Sprintf("%s (%d): %s", l.Name, l.Number, l.Text)
}

// LineIterator is an interface that represents a producer of lines from a file.
type LineIterator interface {
	// Next draws the next value from the iterator. If there are no more lines
	// left in the iterator then an implementation must return IteratorEmptyError.
	Next() (Line, error)
}

// MultiFileLineIterator consumes a FileInfoIterator implementation and produces
// lines from all files produced by the contained iterator.
type MultiFileLineIterator struct {
	fileIterator      FileInfoIterator
	currentScanner    *bufio.Scanner
	currentLineNumber uint64
	currentFileHandle *os.File
	currentFileInfo   os.FileInfo
}

// NewMultiFileLineIterator generates an initalised MultiFileLineIterator
// that reads from the given FileInfoIterator.
func NewMultiFileLineIterator(fileIterator FileInfoIterator) (*MultiFileLineIterator, error) {
	var iterator = MultiFileLineIterator{
		fileIterator:      fileIterator,
		currentScanner:    nil,
		currentFileHandle: nil,
		currentFileInfo:   nil,
		currentLineNumber: 0,
	}
	var err = iterator.fetchNextScanner()
	return &iterator, err
}

func (i *MultiFileLineIterator) fetchNextScanner() error {
	var err error

	if i.currentFileHandle != nil {
		i.currentFileHandle.Close()
	}

	var fileInfo os.FileInfo
	fileInfo, err = i.fileIterator.Next()
	if err != nil {
		return err
	}

	var file *os.File
	file, err = os.Open(fileInfo.Name())
	if err != nil {
		return err
	}

	var scanner = bufio.NewScanner(file)
	i.currentScanner = scanner
	i.currentLineNumber = 0
	i.currentFileHandle = file
	i.currentFileInfo = fileInfo

	return nil
}

// Next produces the next line from one of the files provided by the internal
// iterator object.
func (i *MultiFileLineIterator) Next() (Line, error) {
	var err error

	var ok bool
	ok = i.currentScanner.Scan()
	for !ok {
		err = i.currentScanner.Err()
		if err != nil {
			return Line{}, err
		}
		err = i.fetchNextScanner()
		if err != nil {
			return Line{}, err
		}
		ok = i.currentScanner.Scan()
	}
	i.currentLineNumber = i.currentLineNumber + 1

	var line string
	line = i.currentScanner.Text()

	return Line{
		Name:   i.currentFileInfo.Name(),
		Number: i.currentLineNumber,
		Text:   line,
	}, nil

}

// RegexpLineIteratorWrapper filters all lines from the wrapped LineIterator
// based on a regexp search.
type RegexpLineIteratorWrapper struct {
	pattern  *regexp.Regexp
	iterator LineIterator
}

// NewRegexpLineIteratorWrapper generates a new RegexpLineIteratorWrapper that
// wraps the given LineIterator and filters on the given pattern.
func NewRegexpLineIteratorWrapper(pattern *regexp.Regexp, iterator LineIterator) (*RegexpLineIteratorWrapper, error) {
	return &RegexpLineIteratorWrapper{
		pattern:  pattern,
		iterator: iterator,
	}, nil
}

// Next scrubs through the internal iterator until it finds an entry that
// matches the pattern.
func (i RegexpLineIteratorWrapper) Next() (Line, error) {
	for {
		var line, err = i.iterator.Next()
		if err != nil {
			return Line{}, err
		}
		if i.pattern.FindStringIndex(line.Text) != nil {
			return line, nil
		}
	}
}

// NegativeRegexpLineIteratorWrapper filters all lines from the wrapped LineIterator
// based on not matchign a regexp search.
type NegativeRegexpLineIteratorWrapper struct {
	pattern  *regexp.Regexp
	iterator LineIterator
}

// NewNegativeRegexpLineIteratorWrapper generates a new NegativeRegexpLineIteratorWrapper that
// wraps the given LineIterator and filters on the reverse of the given pattern.
func NewNegativeRegexpLineIteratorWrapper(pattern *regexp.Regexp, iterator LineIterator) (*NegativeRegexpLineIteratorWrapper, error) {
	return &NegativeRegexpLineIteratorWrapper{
		pattern:  pattern,
		iterator: iterator,
	}, nil
}

// Next scrubs through the internal iterator until it finds an entry that
// matches the pattern.
func (i NegativeRegexpLineIteratorWrapper) Next() (Line, error) {
	for {
		var line, err = i.iterator.Next()
		if err != nil {
			return Line{}, err
		}
		if i.pattern.FindStringIndex(line.Text) == nil {
			return line, nil
		}
	}
}
