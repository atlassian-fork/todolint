package todolint

import (
	"os"
)

// IteratorEmptyError represents the error state where the iterator contains no
// more values to return and should not be iterated on any more.
type IteratorEmptyError struct{}

func (e IteratorEmptyError) String() string {
	return "The iterator contains no more items."
}
func (e IteratorEmptyError) Error() string {
	return e.String()
}

// FileInfoIterator is an interface that represents an iterator that produces
// os.FileInfo items.
type FileInfoIterator interface {
	// Next returns the next available value in the iterator. If there are no
	// more values then implementations must return an instance of
	// IteratorEmptyError as the error.
	Next() (os.FileInfo, error)
}

// FileIterator is an implementation of the iterator pattern that consumes a
// directory path and emits only the os.FileInfo objects is the recursive
// directory tree that have IsDir() return true.
type FileIterator struct {
	files    []os.FileInfo
	dirs     []os.FileInfo
	rootPath string
}

// NewFileIter initialises the iterator and sets up the internal state.
func NewFileIter(dirname string) (*FileIterator, error) {
	var fi = FileIterator{
		files:    make([]os.FileInfo, 0),
		dirs:     make([]os.FileInfo, 0),
		rootPath: dirname,
	}
	var err = fi.initialise()
	return &fi, err
}

// initialise sets up the internal state of the iterator based on the dirname
// that was given during New.
func (fi *FileIterator) initialise() error {
	var initialData []os.FileInfo
	var err error
	initialData, err = ReadDir(fi.rootPath)
	if err != nil {
		return err
	}

	for o := range initialData {
		if initialData[o].IsDir() {
			fi.dirs = append(fi.dirs, initialData[o])
			continue
		}
		fi.files = append(fi.files, initialData[o])
	}
	return nil
}

// fetchNextDir shifts the next directory from the internal list
func (fi *FileIterator) fetchNextDir() error {
	if len(fi.dirs) < 1 {
		return IteratorEmptyError{}
	}
	var rootPath os.FileInfo
	rootPath, fi.dirs = fi.dirs[0], fi.dirs[1:]

	var items []os.FileInfo
	var err error
	items, err = ReadDir(rootPath.Name())
	if err != nil {
		return err
	}

	for o := range items {
		if items[o].IsDir() {
			fi.dirs = append(fi.dirs, items[o])
			continue
		}
		fi.files = append(fi.files, items[o])
	}
	return nil
}

// Next returns the next item available in the iterator. The second return
// value will be an instance of IteratorEmptyError if there are no more values
// to return. Otherwise it will represent some os/read error encountered.
func (fi *FileIterator) Next() (os.FileInfo, error) {
	var err error
	if len(fi.files) < 1 && len(fi.dirs) < 1 {
		return nil, IteratorEmptyError{}
	}
	for len(fi.files) < 1 && len(fi.dirs) > 0 {
		err = fi.fetchNextDir()
		if err != nil {
			return nil, err
		}
	}
	if len(fi.files) < 1 {
		return nil, IteratorEmptyError{}
	}

	var result os.FileInfo
	result, fi.files = fi.files[0], fi.files[1:]
	return result, nil
}
