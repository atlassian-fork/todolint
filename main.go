package main

import (
	"flag"
	"fmt"
	"os"
	"regexp"
	"strings"

	todolint "bitbucket.org/atlassian/todolint/lib"
)

type multiFlag []string

func (f multiFlag) String() string {
	var repr = ""
	for o := range f {
		repr = repr + f[o] + ", "
	}
	return repr
}

func (f *multiFlag) Set(value string) error {
	*f = append(*f, value)
	return nil
}

// Lint represents a line that failed the todo test.
type Lint struct {
	Line todolint.Line
	Tag  string
	RHS  string
}

func main() {

	var err error

	var flagSet = flag.NewFlagSet("todolint", flag.ContinueOnError)

	var todoPatterns multiFlag
	var rhsPatterns multiFlag
	flagSet.Var(&todoPatterns, "keyword", "A keyword, like TODO, that indicates a TODO like item.")
	flagSet.Var(&rhsPatterns, "rhs", "A pattern on the right-hand-side of the TODO that indicates lint. Optional parens and spaces are already added.")

	err = flagSet.Parse(os.Args[1:])
	if err != nil {
		panic(err)
	}
	var positionalArgs = flagSet.Args()
	var directoryToLint = "."
	if len(positionalArgs) >= 1 {
		directoryToLint = positionalArgs[0]
	}

	var fileIter todolint.FileInfoIterator
	fileIter, err = todolint.NewFileIter(directoryToLint)
	if err != nil {
		panic(err)
	}

	var lineIter todolint.LineIterator
	lineIter, err = todolint.NewMultiFileLineIterator(fileIter)
	if err != nil {
		panic(err)
	}

	if len(todoPatterns) < 1 {
		todoPatterns = append(todoPatterns, "TODO")
		todoPatterns = append(todoPatterns, "FIXME")
	}
	var todoPattern = fmt.Sprintf("(%s)", strings.Join(todoPatterns, "|"))

	if len(rhsPatterns) < 1 {
		rhsPatterns = append(rhsPatterns, "(^.*)")
	}
	var rhsPattern = fmt.Sprintf(`([\(:])?\W?(%s)\W?([\):])?`, strings.Join(rhsPatterns, "|"))

	var todoRegexp *regexp.Regexp
	todoRegexp, err = regexp.Compile(todoPattern)
	if err != nil {
		panic(err)
	}

	lineIter, err = todolint.NewRegexpLineIteratorWrapper(todoRegexp, lineIter)
	if err != nil {
		panic(err)
	}

	var okRegex *regexp.Regexp
	okRegex, err = regexp.Compile(fmt.Sprintf("%s%s", todoPattern, rhsPattern))
	if err != nil {
		panic(err)
	}

	lineIter, err = todolint.NewNegativeRegexpLineIteratorWrapper(okRegex, lineIter)
	if err != nil {
		panic(err)
	}

	var foundLint = false
	var lint todolint.Line
	for lint, err = lineIter.Next(); err == nil; lint, err = lineIter.Next() {
		foundLint = true
		fmt.Println(lint)
	}

	switch err.(type) {
	case nil:
	case todolint.IteratorEmptyError:
		break
	default:
		panic(err)
	}

	if foundLint {
		os.Exit(1)
	}
	os.Exit(0)
}
