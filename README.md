# todolint #

**A code linter for tracking TODO style comments.**

## How To Use ##

```bash
go get -u bitbucket.org/atlassian/todolint
"${GOPATH}/bin/todolint" <path to repo>
```

### More Options ###

By default, the linter will detect all TODO and FIXME statements in the current
directory. Use the "-keyword" option to customise which words the linter
triggers on:

```bash
"${GOPATH}/bin/todolint" -keyword "TODO" -keyword "FIXME" -keyword "HACK"
```

Additionally, a sequence of "-rhs" flags may be given to allow certain patterns
of TODO statements. For example, the following "-rhs" allows TODO statements
which have a Jira ticket key for the "XYZ" project next to them:

```bash
"${GOPATH}/bin/todolint" -rhs "XYZ-\d+"
```

The above will allow all the following TODO patterns:

  - TODO XYZ-1
  - TODO(XYZ-1)
  - TODO:XYZ-1

### Output Format ###

All detected lint is currently emitted on stderr in the following format:

  <file path> (<line number>): <line with a TODO>

## Contributing ##

### License ###

This project is licensed under Apache 2.0. See LICENSE.txt for details.

### Style ###

This project uses goimports and gometalinter to format.

### Docs ###

All exported items must have documentation.

### Tests ###

If you fix a bug, please add one or more tests to prevent regressions. If you
add a feature, please write tests for as many code branches as possible.

### Contributing Agreement ###

Atlassian requires signing a contributor's agreement before we can accept a
patch. If you are an individual you can fill out the
[individual CLA](https://na2.docusign.net/Member/PowerFormSigning.aspx?PowerFormId=3f94fbdc-2fbe-46ac-b14c-5d152700ae5d).
If you are contributing on behalf of your company then fill out the
[corporate CLA](https://na2.docusign.net/Member/PowerFormSigning.aspx?PowerFormId=e1c17c66-ca4d-4aab-a953-2c231af4a20b).

## TODO - Feature Backlog ##

- Plugins

  Add a plugin system for processing lint. Example use case: Lookup Jira tickets
  add only fail if the ticket status is closed.